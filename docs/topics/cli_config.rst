=====================
CLI_CONFIG Dictionary
=====================

The ``CLI_CONFIG`` dictionary is used to expose a setting on the command-line
of your application so it can be changed. Any values set in ``CLI_CONFIG``
will inherit settings from ``CONFIG``, so that if you have a ``name`` key
in both dictionaries, the final settings for the command-line options will
inherit the ``CONFIG`` settings, too.

All options that appear on the CLI need to be activated in the ``CLI_CONFIG``
but the basic configuration needs to be in the ``CONFIG`` dictionary.

``Pop-config`` uses Python's venerable `argparse` under the hood to present
and process the arguments. Pop-config will also, transparently, passes
options from the dictionary into `argparse`, this makes Pop-config
transparently compatible with new `argparse` options that are made available.

This document is intended, therefore, to present the most commonly used
options, please see the `argparse` doc for more in depth data on available
options.

If the command-line option is very simple it can be as simple as just doing this:

.. code-block:: python

    CLI_CONFIG = {
        "test": {},
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

This would present a command-line option ``--test`` which can be specified
as ``--test=red`` or ``--test blue``.

This document will cover all available options for the ``CLI_CONFIG``,
remember that the `default` and `help` values should always be in the ``CONFIG``
section. This is a POP best practice.

Options
=======

By default the options presented on the command line are identical to the
name of the value. So for the above example the presented option would be
`--test`. If alternative options are desired, they can be easily added:

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "options": ["-t", "--testy-mc-tester", "-Q"],
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }


Positional Arguments
====================

Positional arguments are very common and can create a much more user friendly
experience for users. Adding positional arguments are easy. Just use the
`positional` argument:


.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "positional": True,
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

It would now be possible to specify the argument as ``mycmd foo`` (without
needing a ``--test``).

When working with multiple positional arguments the `display_priority` flag can be
used to control their order:

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "positional": True,
            "display_priority": 2,
        },
        "run": {
            "positional": True,
            "display_priority": 1,
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
        "run": {
            "default": "green",
            "help": "What color to run",
        },
    }

In the above example the first argument will be `run` and the second will be `test`.

Accepting Environment Variables
===============================

Operating systems allow for configuration options to be passed in via
specific means. In Unix based systems like Linux and MacOS, environment variables
can be used. In Windows based systems the registry can be used. To allow
for an os variable to be used just add the ``os`` option:

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "os": "MYAPP_TEST",
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

Now the flag can be set by setting the environment variable ``MYAPP_TEST`` to
the desired configuration value.

Actions
=======

Actions allow a command line argument to perform an action, or flip a switch.

The `action` option passes through to `argparse`, if the examples in this
document do not make sense you can also check the `argparse` section on
`action`.

A number of actions are supported by `argparse`. Arguable the most frequently
used actions are `store_true` and `store_false`:

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "action": "store_true",
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

A few other useful actions are `append` and `count`. If `append` is used then
every time the argument is used the option passed to the argument is appended
to the final list. The `count` option allows for the number of times that the
argument is passed to be counted up. This is useful for situations where you
want to specify what the verbosity of the output should be, so that you can
pass `-vvv` in a similar fashion to ssh.

Number of Arguments
===================

The number of arguments that should be expected can also be set using the
`nargs` option. This allows for a specific or fluid number of options to
be passed into a single cli option.

The `nargs` option passes through to `argparse`, if the examples in this
document do not make sense you can also check the `argsparse` section on
`nargs`.

Integer (1)
-----------

Specifying an integer defines the explicit number of options to require:

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "nargs": 3,
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

The above example will require that exactly 3 options are passed to `--test`.

Question Mark (?)
-----------------

One argument will be consumed from the command line if possible, and produced
as a single item. If no command-line argument is present, the value from
default will be produced.

Asterisk (*)
------------

All command-line arguments present are gathered into a list.

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "nargs": "*",
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

Plus (+)
--------
Just like '*', all command-line args present are gathered into a list.
Additionally, an error message will be generated if there wasn't at
least one command-line argument present.

Type
====

The value type can be enforced with the `type` option. A type can
be passed in that will be enforced, such as `int` or `str`.

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "type": int,
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

Render
======

Sometimes it is desirable to load up complex data structures from the
command line. This can be done with the `render` option. The `render`
option allows you to specify that the argument passed will be rendered
using a data serialization medium such as json or yaml.

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "render": "yaml",
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

This cli could then look like this:

.. code-block:: bash

    myapp --test "Food: true"

Then the resulting value would be: `{"Food": True}`
